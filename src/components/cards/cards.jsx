import Card from "../card";

export default function Cards({ displayName, data }) {    

    return (
        <div className={displayName.toLowerCase()}>
            {
                Array.isArray(data) ? <Card img={null} name={null} species={null} /> : data.results.map((e) => {
                    return <Card key={e.id} img={null} name={e.name} species={e.species || e.type || e.air_date} /> 
                })
            }
        </div>
    )
}