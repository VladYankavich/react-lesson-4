import Navigation from "../navigation";
import logoBlack from "../../image/logo-black.svg";

import "./header.css"

export default function Header({click}) {
    return (
        <div className="header">
            <img src={logoBlack} alt="logoBlack" />
            <Navigation click={click} />
        </div>
    )
}