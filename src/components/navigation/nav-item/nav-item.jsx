import { Link } from "react-router-dom";

export default function NavItem({ click, title, linkName }) {
    

    return (
        <li className="navigation__item" onClick={(e) => {
            const name = title.props.children;
            
            click(e, name.toLowerCase());
        }}>
            <Link to={`/${linkName}`}>
                <div>{title}</div>
            </Link>

        </li>
    );
};