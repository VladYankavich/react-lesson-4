import { useState, useEffect } from "react";
import { req, showName, showLink } from "../../method/method";
import NavItem from "./nav-item";

import "./navigation.css";

export default function Navigation({ click }) {
    const [exchangeName, setExchangeName] = useState([])
    const [exchangeLink, setExchangeLink] = useState([])

    useEffect(() => {
        const data = req("https://rickandmortyapi.com/api");

        data.then((info) => {
            setExchangeName(showName(info))
            setExchangeLink(showLink(info).map((el) => {
                return el.props.children
            }))
        })
    }, [])

    console.log(exchangeLink);
    return (
        <ul className="navigation">
            <NavItem click={click} title={exchangeName[0]} linkName={exchangeLink[0]} />
            <NavItem click={click} title={exchangeName[1]} linkName={exchangeLink[1]} />
            <NavItem click={click} title={exchangeName[2]} linkName={exchangeLink[2]} />
        </ul>
    );
};