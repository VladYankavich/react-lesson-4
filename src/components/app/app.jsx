import { useState } from "react";
import { BrowserRouter } from "react-router-dom";
import { Routes, Route } from "react-router";
import { req } from "../../method/method";
import MainPage from "../../pages/main-page";
import Menu from "../../pages/menu";
import NotFount from "../../pages/notfount"

import "./app.css";

export default function App() {
    const [page, setPage] = useState({status: false});
    const [reqData, setReqData] = useState([]);

    function clickMenu(event, data) {        
        setPage({ display: data, status: true });
        req(`https://rickandmortyapi.com/api/${data}`)
        .then((info) => {
            setReqData(info)
        });
    }
    console.log(page.display);
console.log(reqData);
    return (

        // <>
        //     <MainPage click={clickMenu} />
        //     {
        //         page.status ? <Menu displayName={page.display} data={reqData} /> : null
        //     }
            
        // </>
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<MainPage click={clickMenu} />} />
                <Route path="/character" element={<Menu displayName={page.display} data={reqData} />} />
                <Route path="/location" element={<Menu displayName={page.display} data={reqData} />} />
                <Route path="/episode" element={<Menu displayName={page.display} data={reqData} />} />
                <Route path="*" element={<NotFount />} />
            </Routes>
        </BrowserRouter>        
    );
};