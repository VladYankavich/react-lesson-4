import IconSearch from "../../image/Icon-search.svg";

import "./search-main-page.css";

export default function SearchMainPage() {
    return (
        <div className="search-main-page">
            <div className="search-main-page-box">
                <img src={IconSearch} alt="IconSearch" />
                <input type="text" placeholder="Filter by name or episode"></input>
            </div>

        </div>
    )
}