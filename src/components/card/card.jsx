export default function Card({img, name, species}) {
    return (
        <div className="card">
            <img src={img} alt={name} />
            <h4>{name}</h4>
            <p>{species}</p>
        </div>
    )
}