import rickAndMorty from "../../image/rick-and-morty.png";

export default function MainPageImg() {
    return (
        <div className="main-page__img">
            <img src={rickAndMorty} alt="rickAndMorty" />
        </div>
    )
}