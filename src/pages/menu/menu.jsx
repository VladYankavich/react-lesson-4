import Header from "../../components/header";
import Cards from "../../components/cards/cards";

import "./menu.css"

export default function CharactersPage({click, displayName, data}){
    return (
        <div className="page" >
            <Header click={click} />
            <Cards displayName={displayName} data={data} />
        </div>
    )
}