import Header from "../../components/header";
import MainPageImg from "../../components/main-page-img";
import SearchMainPage from "../../components/search-main-page";

import "./main-page.css";

export default function MainPage({click}) {
    return (
        <div className="page">
            <Header click={click} />
            <MainPageImg /> 
            <SearchMainPage />     
        </div>

    );
};