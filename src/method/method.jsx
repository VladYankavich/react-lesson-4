export async function req(url) {
    const rez = await fetch(url);
    return await rez.json();
};

export function showName(data){
    const arr = Object.keys(data)
    return arr.map((el) => {
        // const name = el.match(/([a-z]*)$/gm)[0][0].toUpperCase()+el.match(/([a-z]*)$/gm)[0].slice(1)
        const name = el[0].toUpperCase()+el.slice(1)
        return <div>{name}</div>
    })
}

export function showLink(data) {
    const arr = Object.values(data)
    return arr.map((el) => {
        const name = el.match(/([a-z]*)$/gm)[0]
        
        return <div>{name}</div>
    })
}